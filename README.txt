There is a service Yandex Site search Pinger (http://site.yandex.ru). This is a
service that allows you to create search on the site or group of sites. For this
service it is necessary that the site has been indexed by Yandex. Robots Yandex
self-index the site. However, apart from the core of the index when searching
the site uses an additional index, specially built for such sites. You can
increase the priority of indexing certain pages of the site with the help of
the plug-in Yandex Site search Pinger, which sends requests to the indexing
automatically. This plug-in installed in a CMS, monitors changes to the site
and generates requests for indexing all new or modified documents.

Basic information on what the plugin-pinger is described on page
http://help.yandex.ru/site/?id=1125183.
Files of plugins and the basic information for the user is located here:
http://site.yandex.ru/cms-plugins/


                               CONFIGURATION

If you are using VCS in your development flow you can configure this module from
your settings.php file in "Variable overrides" section. For example add to $conf
array next values:

$conf = array (
  'yandex_pinger_key' => "__YAKEY__",
  'yandex_pinger_login' => "__YALOGIN__",
  'yandex_pinger_searchId' => "__YASEARCHID__",
  //'yandex_pinger_node_types' => array ('page' => 'page', 'story' => 0,),
);
